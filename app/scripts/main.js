(function() {
  /*global XHR Isotope JST imagesLoaded _ CountUp slug*/
  'use strict';

  var perPage = 24;
  var section, iso, users;

  var avecRepertoire = function () {
    return !!document.querySelector('.avec-repertoire');
  };

  var ficheTeaserDom = function (user) {
    var div = document.createElement('div');
    div.innerHTML = JST.ficheTeaser({user: user});
    if (user.location.city) {
      div.setAttribute('data-city', user.location.city);
    }
    return div;
  };

  var programsIn = function(user, langage) {
    var r;
    if (typeof user.public_repos !== 'object') { return false; }

    for (r in user.public_repos) {
      if (user.public_repos[r].language && user.public_repos[r].language.toLowerCase() === langage) { return true; }
    }
    return false;
  };

  var displayUsers = function() {
    var filteredUsers;
    var sample, numAnim;
    var body = document.getElementsByTagName('body')[0];
    var nFichesEl = document.getElementById('compteFiches');
    var sectionFiches = document.createElement('section');
    var villeInput = slug(document.getElementsByName('ville')[0].value || '');
    var langageInput = document.getElementsByName('langage')[0].value.toLowerCase();
    var hireableInput = document.getElementsByName('hireable')[0].checked;
    var webInput = document.getElementsByName('web')[0].checked;
    var cieInput = document.getElementsByName('cie')[0].checked;
    var emailInput = document.getElementsByName('email')[0].checked;
    var sortInput = document.getElementsByName('sort')[0].value;
    var parts = villeInput.split('-');
    var touched = false;
    var wasN = parseInt(nFichesEl.innerHTML, 10) || 0;

    if (parts[0] === 'st') {
      parts[0] = 'saint';
      touched = true;
    } else if (parts[0] === 'ste') {
      parts[0] = 'sainte';
      touched = true;
    }
    if (touched) { villeInput = parts.join('-'); }

    filteredUsers = users.filter(function (user) {
      return (!villeInput || slug(user.location.city || '') === villeInput) &&
        (!langageInput || programsIn(user, langageInput)) &&
        (!hireableInput || user.hireable) &&
        (!webInput || user.blog) &&
        (!cieInput || user.company) &&
        (!emailInput || user.email);
    });

    switch (sortInput) {
    case 'followers':
      sample = _(filteredUsers)
        .sortBy(function(user) { return user.followers || 0; })
        .reverse()
        .value();
      break;

    case 'repos':
      // le sort est étrange...
      // mais plus important, il n'y a que
      // 30 repos par user dans le json
      // quand il devrait y en avoir beaucoup plus
      sample = _(filteredUsers)
        .sortBy(function(user) {
          return typeof user.public_repos === 'object' ?
            user.public_repos.length :
            0;
        })
        .reverse()
        .value();
      break;

    case 'since':
      sample = _(filteredUsers)
        .sortBy(function(user) { return Date.parse(user.created_at); })
        .value();
      break;

    case 'bof':
    default:
      sample = _.shuffle(filteredUsers);
      break;
    }

    sample = _.slice(sample, 0, perPage);
    nFichesEl.innerHTML = filteredUsers.length;
    if (wasN !== filteredUsers.length) {
      numAnim = new CountUp(
        nFichesEl, wasN, filteredUsers.length, 0, 1 + Math.abs(wasN - filteredUsers.length) / 1500, {
          useEasing: true,
          useGrouping: false
        }
      );
      numAnim.start();
    }
    sample.forEach(function (user) { sectionFiches.appendChild(ficheTeaserDom(user)); });
    section = document.getElementsByTagName('section')[0];
    body.replaceChild(sectionFiches, section);

    // otherwise the grid is all screwed up
    imagesLoaded(sectionFiches, function() {
      section = sectionFiches;
      iso = new Isotope(section);
      // jazz hands!
      iso.shuffle();
    });
  };

  var pageRepertoire = function() {
    var spammers = [
      'JeyInkl',
      'Piston-Drill-Wrong-Hole',
      'Doumlev'
    ];
    XHR.get('/rollodeqc-8000-a.json').then(function (data) {
      var inputs = document.querySelectorAll('.buttons input');
      var sortInput = document.getElementsByName('sort')[0];
      var r;

      // too weird: users is an object in dev, but a string in prod (minified)
      if (typeof data === 'string') { users = JSON.parse(data); }
      else { users = data; }

      users = users.filter(function (user) {
        return user.location &&
          user.location.province === 'quebec' &&
          user.type === 'User' && //Organization
          spammers.indexOf(user.login) === -1;
      });

      displayUsers();
      for (r = 0; r < inputs.length; ++r) { inputs[r].addEventListener('change', displayUsers); }
      sortInput.addEventListener('change', displayUsers);
    });
  };

  if (avecRepertoire()) {
    pageRepertoire();
  }
}());

